# cals #

A simple command line calorie tracking program backed by a local user database.

### Installation ###

Clone the repo, then chmod +x the cals file if necessary.

Make sure you're running python 3:
```
#!bash
python --version
```

If not, check if you have python3:
```
#!bash
python3 --version
```

Install the userdirs package with pip:
```
#!bash
pip install userdirs
```

Run ./cals help or just ./cals to set up the local database.
