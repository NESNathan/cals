#!/usr/bin/env python3

import sys
import os

# Program imports
from .appinfo import appinfo
from .user import user
from .userdb import userdb

# Colour class for terminal output
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

help_string = appinfo.__appname__ + ": Calorie tracker. Version " + appinfo.__version__ + "\n" +\
              "\nAvailable Parameters:\n" +\
              color.BOLD + "\thelp" + color.END + "\t\tShow this help text.\n" +\
              color.BOLD + "\tversion" + color.END + "\t\tShow the version number for this software."

# Database wrapper
db = userdb()

# Current user
current_user = user("", -1, -1, -1)

def main():
    if len(sys.argv) > 1:
        parse_params(sys.argv)
    else:
        # Print daily summary if available, or show setup steps
        validate_user_config()

def parse_params(params):
    if params[1] in ["help", "--help", "-h"]:
        print(help_string)
    elif params[1] in ["version", "--version", "-v"]:
        print(appinfo.__appname__ + " " + appinfo.__version__)
    elif params[1] in ["add", "--add", "-a"]:
        parse_add_calories_params(params)

def validate_user_config():
    global current_user

    if not db.exists():
        print("No database file found for current user")
        start_initial_setup()
    else:
        # Database found, load it and display today's data
        db.connect()
        current_user = db.load_user()

        if current_user == None:
            print("No database entry found for current user")
            start_initial_setup()
        else:
            print_summary_for_date(0) # TODO: Pass today's date

def start_initial_setup():
    print("\nDoing first time initialization.")
    print("Complete the following steps to generate a recommended calorie limit.\n")

    # Create database
    db.create_new_db()

    current_user.name = input("Enter your name: ")
    current_user.height = input("Enter your height in cm: ")
    current_user.weight = input("Enter your weight in lbs: ")
    current_user.calorie_limit = input("Enter your target calorie limit: ")

    print("Saving new user \"" + current_user.name + "\"...")
    db.save_user(current_user)

def print_summary_for_date(date):
    print("Today's log for " + current_user.name)
    print("Today, you did well.")

def parse_add_calories_params(params):
    # Pull out food name, meal type, date and calorie count
    print("Implement me: parse_add_calories_params")

def add_calories_to_date(date, title, calories):
    print("Implement me: add_calories_to_date")

if __name__ == "__main__":
    main()
