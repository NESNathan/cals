import sqlite3

from appdirs import *

from .appinfo import appinfo
from .user import user

# The current user's local database location
user_db_directory = user_config_dir(appinfo.__appname__)
user_db_fullpath = user_db_directory + "/" + "database.db"

class userdb:
    def __init__(self):
        return

    def exists(self):
        return os.path.isdir(user_db_directory) and os.path.exists(user_db_fullpath)

    def connect(self):
        self.connection = sqlite3.connect(user_db_fullpath)
        self.cursor = self.connection.cursor()

    def create_new_db(self):
        # Create directory
        if not os.path.isdir(user_db_directory):
            os.makedirs(user_db_directory)

        # Create file
        open(user_db_fullpath, "a")

        # Get DB connection
        self.connect()

        # Create user table
        self.cursor.execute("CREATE TABLE USER (NAME TEXT, WEIGHT INTEGER, HEIGHT INTEGER, CAL_LIMIT INTEGER)")

        # Create entries table
        self.cursor.execute("CREATE TABLE ENTRIES (CREATED_DATE DATE, NAME TEXT, CALS INTEGER)")

        self.connection.commit()

    def save_user(self, user):
        self.cursor.execute("SELECT * FROM USER")

        row = self.cursor.fetchone()
        if row == None:
            # Insert new user
            self.cursor.execute("INSERT INTO USER VALUES (" \
                                + "'" + user.name + "', " \
                                + str(user.weight) + ", " \
                                + str(user.height) + ", " \
                                + str(user.calorie_limit) \
                                + ")")

            self.connection.commit()
        else:
            # Update user
            print("Implement me!! save_user")

    def load_user(self):
        self.cursor.execute("SELECT * FROM USER")
        row = self.cursor.fetchone()

        if row != None:
            return user(row[0], row[1], row[2], row[3])

        return None
